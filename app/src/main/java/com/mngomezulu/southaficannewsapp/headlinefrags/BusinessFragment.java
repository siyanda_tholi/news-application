package com.mngomezulu.southaficannewsapp.headlinefrags;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mngomezulu.southaficannewsapp.R;
import com.mngomezulu.southaficannewsapp.adapter.HeadlineAdapter;
import com.mngomezulu.southaficannewsapp.adapter.NewsFeedAdapter;
import com.mngomezulu.southaficannewsapp.interfaces.HeadlineOnClickInterface;
import com.mngomezulu.southaficannewsapp.model.ArticleModel;
import com.mngomezulu.southaficannewsapp.util.Constants;
import com.mngomezulu.southaficannewsapp.viewmodel.NewsViewModel;

import java.util.ArrayList;

public class BusinessFragment extends Fragment implements HeadlineOnClickInterface {
    private static final String TAG = "BusinessFragment";
    private RecyclerView recyclerView;
    private NewsViewModel viewModel;
    private HeadlineAdapter newsFeedAdapter;
    private ArrayList<ArticleModel> newsArticle =new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_business, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView =view.findViewById(R.id.business);

        doInit();

    }

    public void doInit(){
        recyclerView.setHasFixedSize(true);
        viewModel = new ViewModelProvider(this).get(NewsViewModel.class);
        newsFeedAdapter =new HeadlineAdapter(newsArticle,getContext(),this);
        recyclerView.setAdapter(newsFeedAdapter);
        newsArticle.clear();
        getBusinessHeadlines();
    }

    private void getBusinessHeadlines() {
        viewModel.getHeadlines("za","business", Constants.apiKey()).observe(getViewLifecycleOwner(),
                headline ->{
                    if (headline != null){
                        if (headline.getArticles() != null){
                            newsArticle.addAll(headline.getArticles());
                            newsFeedAdapter.notifyDataSetChanged();
                        }
                    }
                });
    }

    @Override
    public void share(int position) {
        ArticleModel model =newsArticle.get(position);
        try {
            Intent i =new Intent(Intent.ACTION_SEND);
            i.setType("text/plan");
            i.putExtra(Intent.EXTRA_SUBJECT,model.getSource().getName());
            String body =model.getTitle()+ "\n" + model.getUrl() + "\n" + "Share from the SA News App" + "\n";
            i.putExtra(Intent.EXTRA_TEXT,body);
            startActivity(Intent.createChooser(i,"Share with :"));

        }catch (Exception exception){
            Log.d(TAG, "shareNews: "+exception.getMessage());
        }

    }

    @Override
    public void addBookmark(int position) {
        ArticleModel model =newsArticle.get(position);

    }

    @Override
    public void copyLink(int position) {
    ArticleModel model =newsArticle.get(position);
        ClipboardManager clipboard =(ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("label",model.getUrl());
        clipboard.setPrimaryClip(clip);

        Toast.makeText(getContext(), "Copied to clipboard", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void visitSite(int position) {
        ArticleModel model =newsArticle.get(position);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(model.getUrl()));
        startActivity(intent);
    }
}