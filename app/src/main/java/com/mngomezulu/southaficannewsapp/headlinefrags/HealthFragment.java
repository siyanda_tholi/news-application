package com.mngomezulu.southaficannewsapp.headlinefrags;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mngomezulu.southaficannewsapp.R;
import com.mngomezulu.southaficannewsapp.adapter.HeadlineAdapter;
import com.mngomezulu.southaficannewsapp.interfaces.HeadlineOnClickInterface;
import com.mngomezulu.southaficannewsapp.model.ArticleModel;
import com.mngomezulu.southaficannewsapp.util.Constants;
import com.mngomezulu.southaficannewsapp.viewmodel.NewsViewModel;

import java.util.ArrayList;


public class HealthFragment extends Fragment implements HeadlineOnClickInterface {

    private RecyclerView recyclerView;
    private NewsViewModel viewModel;
    private HeadlineAdapter newsFeedAdapter;
    private ArrayList<ArticleModel> newsArticle =new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_health, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView =view.findViewById(R.id.health);

        doInit();

    }
    public void doInit(){
        recyclerView.setHasFixedSize(true);
        viewModel = new ViewModelProvider(this).get(NewsViewModel.class);
        newsFeedAdapter =new HeadlineAdapter(newsArticle,getContext(),this);
        recyclerView.setAdapter(newsFeedAdapter);
        newsArticle.clear();
        getHealthHeadlines();
    }

    private void getHealthHeadlines() {
        viewModel.getHeadlines("za","health", Constants.apiKey()).observe(getViewLifecycleOwner(),
                headline ->{
                    if (headline != null){
                        if (headline.getArticles() != null){
                            newsArticle.addAll(headline.getArticles());
                            newsFeedAdapter.notifyDataSetChanged();
                        }
                    }
                });
    }

    @Override
    public void share(int position) {

    }

    @Override
    public void addBookmark(int position) {

    }

    @Override
    public void copyLink(int position) {

    }

    @Override
    public void visitSite(int position) {

    }
}