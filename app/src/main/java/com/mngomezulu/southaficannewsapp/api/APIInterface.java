package com.mngomezulu.southaficannewsapp.api;

import com.mngomezulu.southaficannewsapp.model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIInterface {

    @GET("top-headlines")
    Call<ResponseModel> getLatestNews(@Query("country") String country,
                                      @Query("category") String category,
                                      @Query("apiKey") String apiKey);

    @GET("top-headlines")
    Call<ResponseModel> getSAHeadlines(@Query("country") String country,
                                       @Query("apiKey") String apiKey);


    @GET("everything")
    Call<ResponseModel> getEverything(@Query("q") String everythingAbout,
                                      @Query("sortBy") String sortBy,
                                      @Query("language") String language,
                                      @Query("apiKey") String apiKey);

    @GET("top-headlines")
    Call<ResponseModel> headlinesSource(@Query("sources") String sources,
                                        @Query("apiKey") String apiKey);
}
