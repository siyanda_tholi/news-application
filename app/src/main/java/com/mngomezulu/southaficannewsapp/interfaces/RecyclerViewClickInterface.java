package com.mngomezulu.southaficannewsapp.interfaces;

import com.mngomezulu.southaficannewsapp.model.ArticleModel;

public interface RecyclerViewClickInterface {

    void onOptionClick(ArticleModel model);

}
