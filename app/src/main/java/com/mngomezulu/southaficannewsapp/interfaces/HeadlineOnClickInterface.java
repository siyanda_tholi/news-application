package com.mngomezulu.southaficannewsapp.interfaces;

public interface HeadlineOnClickInterface {
    void share(int position);
    void addBookmark(int position);
    void copyLink(int position);
    void visitSite(int position);

}
