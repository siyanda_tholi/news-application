package com.mngomezulu.southaficannewsapp.repository;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.mngomezulu.southaficannewsapp.api.APIInterface;
import com.mngomezulu.southaficannewsapp.api.ApiClient;
import com.mngomezulu.southaficannewsapp.model.ResponseModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsRepository {
    private APIInterface apiService;

    public NewsRepository(){
        apiService = ApiClient.getClient().create(APIInterface.class);
    }

    public LiveData<ResponseModel> getHeadlines(String country, String apiKey){

        MutableLiveData<ResponseModel> data =new MutableLiveData<ResponseModel>();

        apiService.getSAHeadlines(country, apiKey).enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<ResponseModel> call, @NonNull Response<ResponseModel> response) {
                if (response.body() != null){
                    data.setValue(response.body());
                }

            }
            @Override
            public void onFailure(@NonNull Call<ResponseModel> call,@NonNull Throwable t) {
                data.setValue(null);
            }
        });
        return data;
    }

    public LiveData<ResponseModel> getEverything(String q,String sortBy,String language,String apiKey){

        MutableLiveData<ResponseModel> data =new MutableLiveData<>();

        apiService.getEverything(q, sortBy, language, apiKey).enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                data.setValue(null);
            }
        });

        return data;
    }

    public LiveData<ResponseModel> getHeadlinesCategory(String country, String source,String apiKey){

        MutableLiveData<ResponseModel> data =new MutableLiveData<>();

        apiService.getLatestNews(country, source, apiKey).enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {

                data.setValue(null);
            }
        });
        return data;
    }

    public LiveData<ResponseModel> getSourceHeadlines(String source, String apiKey){

        MutableLiveData<ResponseModel> data =new MutableLiveData<>();

        apiService.headlinesSource(source, apiKey).enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                data.setValue(null);
            }
        });
        return data;
    }

}
