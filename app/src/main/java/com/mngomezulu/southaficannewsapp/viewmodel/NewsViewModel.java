package com.mngomezulu.southaficannewsapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.mngomezulu.southaficannewsapp.model.ResponseModel;
import com.mngomezulu.southaficannewsapp.repository.NewsRepository;

public class NewsViewModel extends AndroidViewModel {

private NewsRepository newsRepository;

    public NewsViewModel(@NonNull Application application) {
        super(application);
        newsRepository =new NewsRepository();
    }

    public LiveData<ResponseModel> getHeadlines(String country, String apiKey){
        return newsRepository.getHeadlines(country,apiKey);

    }

    public LiveData<ResponseModel> getEverything(String q,String sortBy,String language,String apiKey){
        return newsRepository.getEverything(q, sortBy, language, apiKey);
    }

    public LiveData<ResponseModel> getSourceHeadlines(String source,String apiKey){
        return newsRepository.getSourceHeadlines(source, apiKey);
    }
    public LiveData<ResponseModel> getHeadlines(String country, String source,String apiKey){
        return newsRepository.getHeadlinesCategory(country, source, apiKey);
    }





}
