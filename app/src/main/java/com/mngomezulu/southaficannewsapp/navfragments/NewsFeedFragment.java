package com.mngomezulu.southaficannewsapp.navfragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.mngomezulu.southaficannewsapp.R;
import com.mngomezulu.southaficannewsapp.activities.MoreNewsActivity;
import com.mngomezulu.southaficannewsapp.activities.SearchActivity;
import com.mngomezulu.southaficannewsapp.adapter.NewsFeedAdapter;
import com.mngomezulu.southaficannewsapp.dialogs.NewsBottomSheetDialog;
import com.mngomezulu.southaficannewsapp.interfaces.RecyclerViewClickInterface;
import com.mngomezulu.southaficannewsapp.model.ArticleModel;
import com.mngomezulu.southaficannewsapp.util.Constants;
import com.mngomezulu.southaficannewsapp.viewmodel.NewsViewModel;

import java.util.ArrayList;


public class NewsFeedFragment extends Fragment implements View.OnClickListener, RecyclerViewClickInterface {

    private static final String FULL_COVERAGE = "trending";
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    ConstraintLayout constraintLayout;
    private NewsViewModel viewModel;
    private ShimmerFrameLayout shimmerFrameLayout;
    private NewsFeedAdapter newsFeedAdapter;
    private ArrayList<ArticleModel> newsArticle =new ArrayList<>();
    private Button corona,vaccine,ramaphosa,telsa;
    private NewsBottomSheetDialog sheetDialog;
    private ImageView search;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news_feed, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        recyclerView = view.findViewById(R.id.trendsRecyclerView);
        refreshLayout =view.findViewById(R.id.swipeRefresh);

        corona =view.findViewById(R.id.covid);
        corona.setOnClickListener(this);

        vaccine =view.findViewById(R.id.vaccine);
        vaccine.setOnClickListener(this);

        ramaphosa =view.findViewById(R.id.ramamphosa);
        ramaphosa.setOnClickListener(this);

        telsa =view.findViewById(R.id.telsa);
        telsa.setOnClickListener(this);

        search =view.findViewById(R.id.search);

        sheetDialog =new NewsBottomSheetDialog();
        shimmerFrameLayout= view.findViewById(R.id.shimmerLayout);
        constraintLayout =view.findViewById(R.id.constraintLayout2);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent =new Intent(getContext(), SearchActivity.class);
                startActivity(intent);
            }
        });

        doInit();
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                newsArticle.clear();
                newsFeedAdapter.notifyDataSetChanged();
                doInit();
                refreshLayout.setRefreshing(false);
            }
        });
    }


    @Override
 public void onClick(View view) {
        switch (view.getId()){

            case R.id.covid:
                Intent intent =new Intent(getContext(), MoreNewsActivity.class);
                intent.putExtra(FULL_COVERAGE,"News24");
                startActivity(intent);
                break;
            case R.id.vaccine:
                Intent intent2 =new Intent(getContext(), MoreNewsActivity.class);
                intent2.putExtra(FULL_COVERAGE,"cnn");
                startActivity(intent2);
                break;
            case R.id.ramamphosa:
                Intent intent3 =new Intent(getContext(), MoreNewsActivity.class);
                intent3.putExtra(FULL_COVERAGE,"bbc-news");
                startActivity(intent3);
                break;
            case R.id.telsa:
                Intent intent4 =new Intent(getContext(), MoreNewsActivity.class);
                intent4.putExtra(FULL_COVERAGE,"the-verge");
                startActivity(intent4);
                break;
        }
    }



    @Override
    public void onOptionClick(ArticleModel model) {

        Bundle article =new Bundle();
        article.putSerializable("model",model);
        article.putString("title",model.getTitle());
        article.putString("date",model.getPublishedAt());
        article.putString("source",model.getSource().getName());
        article.putString("sourceId",model.getSource().getId());
        article.putString("imageUrl",model.getUrlToImage());
        article.putString("url",model.getUrl());
        sheetDialog.setArguments(article);
        sheetDialog.show(getActivity().getSupportFragmentManager(),"sheetDialog");

    }


    public void doInit(){
        recyclerView.setHasFixedSize(true);
        viewModel = new ViewModelProvider(this).get(NewsViewModel.class);
        newsFeedAdapter =new NewsFeedAdapter(newsArticle,getContext(),this);
        recyclerView.setAdapter(newsFeedAdapter);
        getHeadlineNews();
    }
    private void getHeadlineNews(){

        viewModel.getHeadlines("za", Constants.apiKey()).observe(getViewLifecycleOwner(),
                headlinesNews -> {
                    if (headlinesNews !=null){
                        if (headlinesNews.getArticles() != null){
                            shimmerFrameLayout.stopShimmer();
                            shimmerFrameLayout.setVisibility(View.GONE);
                            constraintLayout.setVisibility(View.VISIBLE);
                            newsArticle.addAll(headlinesNews.getArticles());

                            newsFeedAdapter.notifyDataSetChanged();
                        }
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    public void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();

    }
}