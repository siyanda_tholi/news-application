package com.mngomezulu.southaficannewsapp.dialogs;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.mngomezulu.southaficannewsapp.R;
import com.mngomezulu.southaficannewsapp.activities.MoreNewsActivity;
import com.mngomezulu.southaficannewsapp.model.ArticleModel;

import java.util.ArrayList;

public class NewsBottomSheetDialog extends BottomSheetDialogFragment {

    private static final String TAG = "ArticleSheetDialog";
    LinearLayout share, save, moreNews, visitSite, copyLink;
    private ArrayList<ArticleModel> newsArticle = new ArrayList<>();
    private TextView bySource;
    String sourceName;
    String imageUrl, source, date, title, url, sourceId;
    ArticleModel articleModel;
    // holder for now

    @Override
    public int getTheme() {
        return R.style.AppBottomSheetDialogTheme;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.bottom_sheet, null, false);


        share = view.findViewById(R.id.share);
        save = view.findViewById(R.id.save_for_later);
        moreNews = view.findViewById(R.id.more_by_source);
        copyLink = view.findViewById(R.id.copyLink);
        visitSite = view.findViewById(R.id.link);

        bySource = view.findViewById(R.id.source_name);

        initArguments();

        sourceName = source.substring(0, 1).toUpperCase() + source.substring(1).toLowerCase();
        bySource.setText("More by " + sourceName);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Change this is just for code check
                shareNews();
                dismiss();
            }
        });
        moreNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moreBySource();
            }
        });

        copyLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    copyToClipBoard();
                    dismiss();
                    Toast.makeText(getActivity(), "Link copied to clipboard", Toast.LENGTH_SHORT).show();

                } catch (Exception exception) {
                    Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_SHORT).show();
                }
                copyToClipBoard();

            }
        });

        visitSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    visitSite();
                    dismiss();
                } catch (Exception exception) {
                    Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_SHORT).show();
                    dismiss();
                }


            }
        });
        return view;
    }

    private void shareNews() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plan");
            i.putExtra(Intent.EXTRA_SUBJECT, source);
            String body = title + "\n" + url + "\n" + "Share from the SA News App" + "\n";
            i.putExtra(Intent.EXTRA_TEXT, body);
            startActivity(Intent.createChooser(i, "Share with :"));

        } catch (Exception exception) {
            Log.d(TAG, "shareNews: " + exception.getMessage());
        }
    }

    private void moreBySource() {
        Intent intent = new Intent(getContext(), MoreNewsActivity.class);
        intent.putExtra("source", sourceName);
        startActivity(intent);
    }

    private void initArguments() {
        source = getArguments().getString("source");
        imageUrl = getArguments().getString("imageUrl");
        date = getArguments().getString("date");
        title = getArguments().getString("title");
        url = getArguments().getString("url");
        sourceId = getArguments().getString("sourceId");
        articleModel = (ArticleModel) getArguments().getSerializable("model");
    }

    private void copyToClipBoard() {
        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("label", url);
        clipboard.setPrimaryClip(clip);
    }

    private void visitSite() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }

}
