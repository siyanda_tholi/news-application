package com.mngomezulu.southaficannewsapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.mngomezulu.southaficannewsapp.R;
import com.mngomezulu.southaficannewsapp.activities.WebViewActivity;
import com.mngomezulu.southaficannewsapp.interfaces.HeadlineOnClickInterface;
import com.mngomezulu.southaficannewsapp.model.ArticleModel;
import com.mngomezulu.southaficannewsapp.util.DateTimeConverter;

import java.util.ArrayList;

public class HeadlineAdapter extends RecyclerView.Adapter<HeadlineAdapter.HeadlineViewHolder> {

    ArrayList<ArticleModel> articleModels;
    Context context;
    private HeadlineOnClickInterface clickInterface;

    public HeadlineAdapter(ArrayList<ArticleModel> articleModels, Context context, HeadlineOnClickInterface clickInterface) {
        this.articleModels = articleModels;
        this.context = context;
        this.clickInterface = clickInterface;
    }


    @NonNull
    @Override
    public HeadlineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HeadlineViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.headline_article, parent,
                        false
                )
        );
    }

    @Override
    public void onBindViewHolder(@NonNull HeadlineViewHolder holder, int position) {

        ArticleModel model =articleModels.get(position);

        if (!TextUtils.isEmpty(model.getPublishedAt())) {
            holder.time.setText(" \u2022 " + DateTimeConverter.DateToTimeFormat(model.getPublishedAt()));
        }
        if (!TextUtils.isEmpty(model.getTitle())){
            holder.title.setText(model.getTitle());
        }
        if (!TextUtils.isEmpty(model.getSource().getName())){
            holder.source.setText(model.getSource().getName());
        }
        if (!TextUtils.isEmpty(model.getDescription())){
            holder.description.setText(model.getDescription());
        }

        Glide.with(context)
                .load(model.getUrlToImage())
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.RESOURCE))
                .error(R.color.shimmer_color)
                .into(holder.image);

        holder.rootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        if (!TextUtils.isEmpty(model.getUrl())) {
                            Log.e("clicked url", model.getUrl());
                            Intent webActivity = new Intent(context, WebViewActivity.class);
                            webActivity.putExtra("url", model.getUrl());
                            webActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(webActivity);
                        }
            }
        });

    }

    @Override
    public int getItemCount() {
        return articleModels.size();
    }

    class HeadlineViewHolder extends RecyclerView.ViewHolder {
        RoundedImageView image;
        TextView source, title, time,description;
        CardView rootLayout;
        ImageView share,bookmarks,link,copyLik;

        public HeadlineViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            source = itemView.findViewById(R.id.source);
            title = itemView.findViewById(R.id.title);
            time = itemView.findViewById(R.id.time);

            description =itemView.findViewById(R.id.description);

            rootLayout = itemView.findViewById(R.id.headlineRoot);

            share =itemView.findViewById(R.id.share);
            bookmarks =itemView.findViewById(R.id.bookmarks);
            link =itemView.findViewById(R.id.visit);
            copyLik =itemView.findViewById(R.id.copyLink);

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickInterface != null){
                        int position  = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            clickInterface.share(position);
                        }
                    }
                }

            });
            bookmarks.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickInterface != null){
                        int position  = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            clickInterface.share(position);
                        }
                    }
                }

            });
            link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickInterface != null){
                        int position  = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            clickInterface.visitSite(position);
                        }
                    }
                }

            });
            copyLik.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickInterface != null){
                        int position  = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            clickInterface.copyLink(position);
                        }
                    }
                }

            });
        }

    }
}
