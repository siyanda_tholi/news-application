package com.mngomezulu.southaficannewsapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.mngomezulu.southaficannewsapp.R;
import com.mngomezulu.southaficannewsapp.activities.WebViewActivity;
import com.mngomezulu.southaficannewsapp.interfaces.RecyclerViewClickInterface;
import com.mngomezulu.southaficannewsapp.model.ArticleModel;
import com.mngomezulu.southaficannewsapp.util.DateTimeConverter;

import java.util.ArrayList;

public class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.NewsFeedViewHolder> {
    ArrayList<ArticleModel> articleModels;
    Context context;
    private RecyclerViewClickInterface clickInterface;

    public NewsFeedAdapter(ArrayList<ArticleModel> articleModels, Context context,RecyclerViewClickInterface clickInterface) {
        this.articleModels = articleModels;
        this.context =context;
        this.clickInterface =clickInterface;
    }


    @NonNull
    @Override
    public NewsFeedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NewsFeedViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.main_article_layout, parent,
                        false
                )
        );
    }

    @Override
    public void onBindViewHolder(@NonNull NewsFeedViewHolder holder, int position) {
        ArticleModel model =articleModels.get(position);

        if (!TextUtils.isEmpty(model.getPublishedAt())) {
            holder.time.setText(" \u2022 " + DateTimeConverter.DateToTimeFormat(model.getPublishedAt()));
        }
        if (!TextUtils.isEmpty(model.getTitle())){
            holder.title.setText(model.getTitle());
        }
        if (!TextUtils.isEmpty(model.getSource().getName())){
            holder.source.setText(model.getSource().getName());
        }


        Glide.with(context)
                .load(model.getUrlToImage())
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.RESOURCE))
                .error(R.color.shimmer_color)
                .into(holder.image);

        holder.rootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        if (!TextUtils.isEmpty(model.getUrl())) {
                            Log.e("clicked url", model.getUrl());
                            Intent webActivity = new Intent(context, WebViewActivity.class);
                            webActivity.putExtra("url", model.getUrl());
                            webActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(webActivity);
                        }

            }
        });
    }

    @Override
    public int getItemCount() {
        return articleModels.size();
    }

    class NewsFeedViewHolder extends RecyclerView.ViewHolder {

        RoundedImageView image;
        TextView source, title, time;
        LinearLayout rootLayout;
        ImageView option;

        public NewsFeedViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.imageUrl);
            source = itemView.findViewById(R.id.source);
            title = itemView.findViewById(R.id.title);
            time = itemView.findViewById(R.id.time);
            rootLayout = itemView.findViewById(R.id.rootLayout);
            option =itemView.findViewById(R.id.option);

            option.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickInterface != null){
                      ArticleModel model = articleModels.get(getAdapterPosition());
                        clickInterface.onOptionClick(model);
                        }
                    }

            });

        }
    }
}
