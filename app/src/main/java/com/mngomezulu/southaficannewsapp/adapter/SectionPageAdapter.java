package com.mngomezulu.southaficannewsapp.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.mngomezulu.southaficannewsapp.R;
import com.mngomezulu.southaficannewsapp.headlinefrags.BusinessFragment;
import com.mngomezulu.southaficannewsapp.headlinefrags.EntertainmentFragment;
import com.mngomezulu.southaficannewsapp.headlinefrags.HealthFragment;
import com.mngomezulu.southaficannewsapp.headlinefrags.ScienceFragment;
import com.mngomezulu.southaficannewsapp.headlinefrags.SportFragment;
import com.mngomezulu.southaficannewsapp.headlinefrags.TechnologyFragment;

public class SectionPageAdapter extends FragmentPagerAdapter {
    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.business,R.string.health,R.string.science,R.string.tech,R.string.sport,R.string.entertain};
    private final Context mContext;

    public SectionPageAdapter(Context context,@NonNull FragmentManager fm) {
        super(fm);
        mContext =context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        switch (position){
            case 0:
                fragment =new BusinessFragment();
                break;
            case 1:
                fragment =new HealthFragment();
                break;
            case 2:
                fragment =new ScienceFragment();
                break;
            case 3:
                fragment =new TechnologyFragment();
                break;
            case 4:
                fragment =new SportFragment();
                break;
            case 5:
                fragment =new EntertainmentFragment();
                break;
        }
        return fragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        return 6;
    }
}
