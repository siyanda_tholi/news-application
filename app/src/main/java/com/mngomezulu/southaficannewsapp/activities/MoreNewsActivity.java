package com.mngomezulu.southaficannewsapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.mngomezulu.southaficannewsapp.R;
import com.mngomezulu.southaficannewsapp.adapter.HeadlineAdapter;
import com.mngomezulu.southaficannewsapp.interfaces.HeadlineOnClickInterface;
import com.mngomezulu.southaficannewsapp.model.ArticleModel;
import com.mngomezulu.southaficannewsapp.navfragments.NewsFeedFragment;
import com.mngomezulu.southaficannewsapp.util.Constants;
import com.mngomezulu.southaficannewsapp.viewmodel.NewsViewModel;

import java.util.ArrayList;

public class MoreNewsActivity extends AppCompatActivity implements HeadlineOnClickInterface {

    private static final String TAG = "BusinessFragment";
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private NewsViewModel viewModel;
    private HeadlineAdapter newsFeedAdapter;
    private ArrayList<ArticleModel> newsArticle =new ArrayList<>();
    private String sourceName;
    private ShimmerFrameLayout shimmerFrameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_news);

        recyclerView =findViewById(R.id.moreNews);
        ImageView navigateBack = findViewById(R.id.navigate_back);
        shimmerFrameLayout = findViewById(R.id.shimmerLayout2);
        swipeRefreshLayout =findViewById(R.id.swipeRefresh);

        Intent source =getIntent();
        sourceName  = source.getStringExtra("trending");

        navigateBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });



        doInit();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                newsArticle.clear();
                newsFeedAdapter.notifyDataSetChanged();
                doInit();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    public void doInit(){
        recyclerView.setHasFixedSize(true);
        viewModel = new ViewModelProvider(this).get(NewsViewModel.class);
        newsFeedAdapter =new HeadlineAdapter(newsArticle,getApplicationContext(),this);
        recyclerView.setAdapter(newsFeedAdapter);
        newsArticle.clear();
        getBusinessHeadlines();
    }

    private void getBusinessHeadlines() {
        viewModel.getSourceHeadlines(sourceName,Constants.apiKey()).observe(this,
                sourceNews ->{
                    if (sourceNews != null){
                        if (sourceNews.getArticles() != null){
                            shimmerFrameLayout.stopShimmer();
                            shimmerFrameLayout.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            newsArticle.addAll(sourceNews.getArticles());
                            newsFeedAdapter.notifyDataSetChanged();
                        }

                    }
                });
    }

    @Override
    public void share(int position) {

    }

    @Override
    public void addBookmark(int position) {

    }

    @Override
    public void copyLink(int position) {

    }

    @Override
    public void visitSite(int position) {

    }
    @Override
    public void onStart() {
        super.onStart();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    public void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();

    }
}