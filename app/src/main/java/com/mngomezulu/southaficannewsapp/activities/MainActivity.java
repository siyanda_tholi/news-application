package com.mngomezulu.southaficannewsapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.ismaeldivita.chipnavigation.ChipNavigationBar;
import com.mngomezulu.southaficannewsapp.R;
import com.mngomezulu.southaficannewsapp.navfragments.BookmarksFragment;
import com.mngomezulu.southaficannewsapp.navfragments.HeadlinesFragment;
import com.mngomezulu.southaficannewsapp.navfragments.NewsFeedFragment;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Main activity";
    ChipNavigationBar bottomNav;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        bottomNav =findViewById(R.id.bottom_navbar);
        if (savedInstanceState == null){
            bottomNav.setItemSelected(R.id.home,true);
            fragmentManager  =getSupportFragmentManager();
            NewsFeedFragment dashboard = new NewsFeedFragment();
            fragmentManager.beginTransaction().replace(R.id.fragment_container,dashboard)
                    .commit();
        }

        bottomNav.setOnItemSelectedListener(new ChipNavigationBar.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int id) {
                Fragment fragment = null;

                switch (id) {
                    case R.id.home:
                        fragment =new NewsFeedFragment();
                        break;
                    case R.id.headline:
                        fragment = new HeadlinesFragment();
                        break;
                    case R.id.bookmarks:
                        fragment =new BookmarksFragment();
                        break;
                }
                if (fragment!=null){
                    fragmentManager =getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.fragment_container,fragment)
                            .commit();
                }else {
                    Log.d(TAG, "onItemSelected: ");
                }

            }
        });
    }

    }
